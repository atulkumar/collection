package com.lara;

import java.util.ArrayList;

public class Manager10
{
	public static void main(String[] args)
	{
		ArrayList list = new ArrayList();//creating object
		list.add(9);//adding element into list
		list.add(0);
		list.add(4);
		list.add(6);
		list.add(8);
		list.add(2);
		list.add(1);
		list.add(3);
		System.out.println(list);
		boolean flag = list.remove(new Integer(2));//value remove:2
		System.out.println(list);
		System.out.println(flag);
		flag = list.remove(new Integer(12));//if value not available in Array return false
		System.out.println(flag);
	}
}