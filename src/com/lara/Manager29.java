package com.lara;

import java.util.ArrayList;
import java.util.Collections;

public class Manager29
{
	public static void main(String[] args)
	{
		ArrayList list = new ArrayList();
		list.add(9);
		list.add(0);
		list.add(5);
		list.add(4);
		list.add(19);
		list.add(90);
		list.add(29);
		list.add(5);
		System.out.println(list);
		System.out.println("max value is:"+Collections.max(list));
		System.out.println("min value is:"+Collections.min(list));
	}
}/*while choosing array to store multiple elements there are some limitations-
*size can not be changed after defining array
*can store only homogeneous elements
//have to specify index explicitly always for storing and reading the elements,it is difficult if size of array is bigger
//processing or managing array elements are very difficult

//In order to address these difficulties java having collection API
//String/StringBuffer/StringBuilder---Built in methods

//collection API divided into 4 parts-
* List-maintaining order through index
 * SET- where maintaining unique, duplicates are not allowed
 * Queue-Stack
 * Map-Key value pairs storing

* List-3 classes
 * ArrayList
 * LinkedList
 * Vector
 
*set-
 * linked HashSet       * HashSet          * TreeSet

*Queue-
 * LinkedList--type of list and also queue       * PriorityQueue

*Map-
 * HashMap     * HashTable       * LinkedHashMap       * TreeMap

*enumeration,iterator,list iterator comparable,comparator*/