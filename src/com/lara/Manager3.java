package com.lara;

import java.util.ArrayList;

public class Manager3
{
	public static void main(String[] args)
	{
		ArrayList list = new ArrayList();//creating object
		list.add(90);//adding element into list
		list.add("abc");
		list.add(90.9);
		list.add(true);
		System.out.println(list.size());//return size
		for(int i = 0; i < list.size(); i++)
		{
			System.out.println(list.get(i));//list obj.
		}
	}
}//in case of collection API size() is used for length of array list